// config-overrides.js

module.exports = function override(config) {
    // Add the necessary resolve.fallback configuration to address the "timers" module issue
    config.resolve.fallback = {
      ...config.resolve.fallback,
      timers: require.resolve('timers-browserify'),
      stream: require.resolve("stream-browserify")
    };
  
    return config;
  };
  