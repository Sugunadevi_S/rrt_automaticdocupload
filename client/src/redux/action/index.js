import config from '../../config';
import axios from 'axios';

export const FETCH_FOLDER_NAME = 'FETCH_FOLDER_NAME';
export const FETCH_JSON_RESPONSE = 'FETCH_JSON_RESPONSE';
export const FETCH_REGRESSION_RESPONSE = 'FETCH_REGRESSION_RESPONSE';
export const FETCH_FILE_RESPONSE = 'FETCH_FILE_RESPONSE';

/** api call for fetching all the file names from the sharepoint */
export const fetchFolderNames = () => async (dispatch) => {
  try {
    const response = await axios.get(`${config.serviceUrl}${config.getAllFolders}`);
    console.log("response", response.data.folders);
    dispatch({
      type: FETCH_FOLDER_NAME,
      payload: response.data.folders.map((items) => {return items.Name;})
    });
    return response; // You can return the entire response object if needed
  } catch (error) {
    console.error('Error:', error);
  }
};

/** upload document to the sharepoint */
export const uploadDocument = (targetFolder) => async (dispatch) => {
  try {
    const response = await axios.post(`${config.serviceUrl}documentUpload/${targetFolder}`);
    return response.data.result; 
  } catch (error) {
    console.error('Error:', error);
  }
};

/** getting json reponse from all the xml files */
export const getXmltoJsonResponse = () => async (dispatch) => {
  try {
    const response = await axios.get(`${config.serviceUrl}${config.getJsonResponse}`);
    dispatch({
      type: FETCH_JSON_RESPONSE,
      payload: response.data.result
    });
    return response.data.result;
  } catch (error) {
    console.error('Error:', error);
  }
};

export const getRegressionResponse = () => async (dispatch) => {
  try {
    const response = await axios.get(`${config.serviceUrl}${config.getRegressionResponse}`);
    dispatch({
      type: FETCH_REGRESSION_RESPONSE,
      payload: response.data.result
    });
    console.log("response.data.result", response.data.result);
    return response.data.result;
  } catch (error) {
    console.error('Error:', error);
  }
};

/** Save the extracted data to the database */
export const saveExtractedData = (value) => async (dispatch) => {
  let body =  JSON.stringify(value);
  axios
  .post(config.serviceUrl + config.insertData, body, {
    headers: {
      "Content-Type": "application/json",
    },
  }).then((res) => res.data.result).catch((err) => err.response?.data);
}

/** Save the extracted data to the database */
export const sendingMail = (value) => async (dispatch) => {
  console.log(value);
  let body =  JSON.stringify(value);
  axios
  .post(config.serviceUrl + config.mailData, body, {
    headers: {
      "Content-Type": "application/json",
    },
  }).then((res) => res.data.result).catch((err) => err.response?.data);
}

export const getJsonResponse = () => async (dispatch) => {
  try {
    const response = await axios.get(`${config.serviceUrl}${config.getFileResponse}`);
    dispatch({
      type: FETCH_FILE_RESPONSE,
      payload: response.data.result
    });
    console.log("response1", response)
    return response.data.jsonData;
  } catch (error) {
    console.error('Error:', error);
  }
};

/** Save the extracted data to the database */
export const saveTestCaseData = (value) => async (dispatch) => {
  let body =  JSON.stringify(value);
  axios
  .post(config.serviceUrl + config.insertTestCase, body, {
    headers: {
      "Content-Type": "application/json",
    },
  }).then((res) =>console.log(res.data.result)).catch((err) => err.response?.data);
}

