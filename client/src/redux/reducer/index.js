import { combineReducers } from 'redux';
import { FETCH_FOLDER_NAME, FETCH_JSON_RESPONSE, FETCH_REGRESSION_RESPONSE, FETCH_FILE_RESPONSE } from '../action/index';

// Define the initial state for this reducer
const initialState = {
  allFolderNames: [],
  jsonResponse: {},
  regressionResponse: {},
  jsonFileResponse: {}
};

// Define the reducer function
const folderNamesReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_FOLDER_NAME:
      return {
        ...state,
        allFolderNames: action.payload,
      };
      case FETCH_JSON_RESPONSE:
        return{
          ...state,
          jsonResponse: action.payload,
        };
      case FETCH_REGRESSION_RESPONSE:
        return{
          ...state,
          regressionResponse: action.payload,
        };
      case FETCH_FILE_RESPONSE:
        return{
          ...state,
          jsonFileResponse: action.payload,
        }; 
    default:
      return state;
  }
};

// Export the reducer using combineReducers
export default combineReducers({
  folderNames: folderNamesReducer,
});
