import React, { useEffect, useState } from 'react';
import "bootstrap/dist/css/bootstrap.min.css";
import './App.css';
import { useDispatch, useSelector} from 'react-redux';
import {fetchFolderNames, uploadDocument} from './redux/action/index';
import XMLDisplay from './component/xml2json';

function App() {

  const dispatch = useDispatch(); // Get the dispatch function
  const folderNames = useSelector((state) => state.folderNames.allFolderNames);

  const [releaseFolder, setReleaseFolder] = useState('');

  useEffect(() => {
    // Dispatch the action
    dispatch(fetchFolderNames());
  }, [dispatch]);

  const handleSelect = (event) => {
    setReleaseFolder(event.target.value)
  }

  const handleUpload = async() => {
    const result = await(dispatch(uploadDocument(releaseFolder)));
    alert(result);
  }

  return (
    <div className="App">
      <h3 className='mb-5'>Documents Upload to sharepoint</h3>
      <div className='d-flex justify-content-center mb-5'>
        <label className='m-3 text-nowrap d-flex align-items-center'>Select Folder : </label>
        <select className="mt-3 mb-3 w-40 form-select" aria-label="Default select example" onChange={handleSelect}>
          {folderNames.map((option, index) => (
              <option key={index} value={option}>
                {option}
              </option>
            ))}
        </select>
        <button className='btn btn-primary m-3' onClick={handleUpload}>Upload</button>
      </div>
      
      <div>
        <XMLDisplay />
      </div>  
    </div>
  );
}

export default App;
