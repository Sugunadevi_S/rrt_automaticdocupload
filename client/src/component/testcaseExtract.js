import React from 'react';
import { useDispatch} from 'react-redux';
import {getRegressionResponse, saveTestCaseData} from '../redux/action/index';

const testcaseExtract = () => {
   const dispatch = useDispatch();

   /** make the excel formatted data from json response */
   const exportToExcelForRegressionPack = async (exportData) => {
    if (exportData.length === 0) {
        return "Failure";
    }
    console.log("exportData",exportData);
    const flattenedData = exportData.flatMap(items => items.map(item => item.$));
    const testCaseData = exportData.flatMap(items => items.map(data => data.testcase));
    const results = [];
    
    const transformItem = (item, data) => {
      const nameParts = item.name.split('-');
      console.log("nameParts", nameParts);
      // console.log('data', data);
      const resultsData = data?.map((testData) => {
         //return testData;
         const extractDay = testData.$.name.split("|")[2].split("-")[1];
         const testcaseName = testData.$.name.replace(/-Day1/g, "").replace(/-Day2/g, "");
        if (nameParts[1] === "Smoke Test" || nameParts[1] === "Regression"|| nameParts[1] === " Full Regression ") {
          if (testData.failure) {
            //console.log("data", data.$.name);
            
            return {
              Domain: nameParts[1],
              Type: (nameParts[0] ==="Regression") ? "Full Regression" : (nameParts[0] ===" Full Regression " && nameParts[2] === " Life Connect") ? "Smoke Test" : nameParts[0],
              Application: nameParts[2],
              TestCase: testcaseName,
              Status: "Failed",
              Environment: "ACCP01",
            };
          } 
          else {
            return {
              Domain: nameParts[1],
              Type: (nameParts[0] ==="Regression") ? "Full Regression" : (nameParts[0] ===" Full Regression " && nameParts[2] === " Life Connect") ? "Smoke Test" : nameParts[0],
              Application: nameParts[2],
              TestCase: testcaseName,
              Status: (extractDay=="Day1") ? "Inprogress" : "Passed",
              Environment: "ACCP01",
            };
          }
        }
        else if (nameParts[2] == "Product_VDFIN") {
          if (testData.failure) {
            return {
              Domain: nameParts[1],
              Type: nameParts[0],
              Application: nameParts[2],
              TestCase: testcaseName,
              Status: "Failed",
              Environment: "PROD",
            };
          } else {
            return {
              Domain: nameParts[1],
              Type: nameParts[0],
              Application: nameParts[2],
              TestCase: testcaseName,
              Status: (extractDay=="Day1") ? "Inprogress" : "Passed",
              Environment: "PROD",
            };
          }
        }
        // console.log("testData", testData);
        const environmentName = testData.$.name.split("|")[2].split("-")[0].trim();
        if (testData.failure){
          return {
            Domain: nameParts[1],
            Type: nameParts[0],
            Application: nameParts[2],
            TestCase: testcaseName,
            Status: "Failed",
            Environment: (environmentName == "ACCP") ? "ACCP01" : environmentName,
          }
        } else {
          return {
            Domain: nameParts[1],
            Type: nameParts[0],
            Application: nameParts[2],
            TestCase: testcaseName,
            Status: (extractDay=="Day1") ? "Inprogress" : "Passed",
            Environment: (environmentName == "ACCP") ? "ACCP01" : environmentName,
          }
        }
      })
      //console.log("resultsData", resultsData);
      return resultsData;
    };
    //console.log("transformItem", transformItem);
    const splittedItems = flattenedData.map((flattenedItem, index) => {
      const testCaseItem = testCaseData[index];
      return transformItem(flattenedItem, testCaseItem);
    });
  
    const finalArray = splittedItems.flat();
    console.log("finalArray", finalArray);
    
    await dispatch(saveTestCaseData(finalArray));
   // const generatedResult = generateExcelForTestCases(finalArray);
    //const result = (generatedResult === "Success") ? "Success" : "Failure";
    return "success";
  };

   const handleExport = async() =>{
    const jsonResponse = await dispatch(getRegressionResponse());
      const convertedData = await exportToExcelForRegressionPack(jsonResponse);
      console.log("jsonResponse", convertedData);
   } 
  return (
    <>
    <button className="btn btn-success m-3" onClick={() => handleExport("regression")}>Export Test Details </button>
    </>
  )
}

export default testcaseExtract