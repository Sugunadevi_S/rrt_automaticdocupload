import React from 'react';
import { useDispatch} from 'react-redux';
import {getXmltoJsonResponse,getRegressionResponse, saveExtractedData} from '../redux/action/index';
import XLSX from 'sheetjs-style';
import TestcaseExtract from './testcaseExtract';


const XMLDisplay = () => {
  
  const dispatch = useDispatch();

  /* Date Formatting Function */
  const ValidateDate = (data) => {
    if (data < 10) {
      data = "0" + data;
    }
    return data;
  };

  /** once we got extracted data this function will generate the excel report */
  const generateExcel = (extractData, smokeTestMergeSize) => {
    if (extractData.length === 0) return "Failure";
  
    const workbook = XLSX.utils.book_new();
    const worksheet = XLSX.utils.json_to_sheet(extractData);
  
    worksheet['!merges'] = [{ s: { r: 1, c: 1 }, e: { r: smokeTestMergeSize, c: 1 } }];
  
    XLSX.utils.book_append_sheet(workbook, worksheet, "AutomaticReport");
  
    const today = new Date();
    const date = `${ValidateDate(today.getDate())}${ValidateDate(today.getMonth() + 1)}${today.getFullYear()}`;
  
    const fileName = `Automatic Excel Report - ${date}.xlsx`;
  
    XLSX.writeFile(workbook, fileName);
    return "Success";
  };

  const generateExcelForTestCases = (extractData) => {
    if (extractData.length === 0) return "Failure";
  
    const workbook = XLSX.utils.book_new();
    const worksheet = XLSX.utils.json_to_sheet(extractData);
  
    XLSX.utils.book_append_sheet(workbook, worksheet, "AutomaticReport");
  
    const today = new Date();
    const date = `${ValidateDate(today.getDate())}${ValidateDate(today.getMonth() + 1)}${today.getFullYear()}`;
  
    const fileName = `Automatic Excel Report - ${date}.xlsx`;
  
    XLSX.writeFile(workbook, fileName);
    return "Success";
  };

  /** Format the smoke test data */
  const processSmokeTestData = (data) => {
    const groupedData = {};
  
    data.forEach(entry => {
      const { Application, NoOfCase, NoOfPass, NoOfFail } = entry;
  
      if (!groupedData[Application]) {
        groupedData[Application] = { ...entry };
      } else {
        groupedData[Application].NoOfCase += parseInt(NoOfCase, 10);
        groupedData[Application].NoOfPass += parseInt(NoOfPass, 10);
        groupedData[Application].NoOfFail += parseInt(NoOfFail, 10);
      }
    });
    
    return Object.values(groupedData);
  };

  /** formatting the health check data */
  const processHealthCheckData = (data) => {
    const groupedData = {};
  
    data.forEach(entry => {
      const { Application, NoOfCase, NoOfPass, NoOfFail } = entry;
  
      if (!groupedData[Application]) {
        groupedData[Application] = { ...entry };
      } else {
        groupedData[Application].NoOfCase += parseInt(NoOfCase, 10);
        groupedData[Application].NoOfPass += parseInt(NoOfPass, 10);
        groupedData[Application].NoOfFail += parseInt(NoOfFail, 10);
      }
    });
    
    return Object.values(groupedData);
  };
  
  /** make the excel formatted data from json response */
  const exportToExcel = async (exportData) => {
    if (exportData.length === 0) {
        return "Failure";
    }

    const flattenedData = exportData.flatMap(items => items.map(item => item.$));

    const transformItem = item => {
        const nameParts = item.name.split('-');
        return {
            Domain: nameParts[0],
            Type: nameParts[1],
            Application: nameParts[2],
            NoOfCase: (nameParts[2] === "Cap & Floor") ? 1 : parseInt(item.tests),
            NoOfPass: (nameParts[2] === "Cap & Floor") ? (parseInt(item.tests) !== 0 ? 1 : 0) : item.tests - item.failures,
            NoOfFail: (nameParts[2] === "Cap & Floor") ? (parseInt(item.tests) !== 0 ? 1 : 0) : parseInt(item.failures),
        };
    };

    const splittedItems = flattenedData.map(transformItem);

    const separateDomains = (data, domainArray, condition) => {
        data.filter(condition).forEach(item => domainArray.push(item));
    };

    const smokeTestDomain = [];
    const healthCheckDomain = [];

    splittedItems.forEach(data => {
        if (data.Application === "Cap & Floor") {
            data.NoOfCase = 1;
            data.NoOfPass = (data.NoOfPass !== 0) ? 1 : 0;
            data.NoOfFail = (data.NoOfFail !== 0) ? 0 : 1;
        }

        separateDomains([data], smokeTestDomain, d => d.Type === "Smoke Test" || d.Type === "SmokeTest");
        separateDomains([data], healthCheckDomain, d => !(d.Type === "Smoke Test" || d.Type === "SmokeTest"));
    });

    const groupSmokeTestDomain = processSmokeTestData(smokeTestDomain);
    const groupHealthCheckDomain = processHealthCheckData(healthCheckDomain);

    const finalArray = [...groupSmokeTestDomain, ...groupHealthCheckDomain];
    console.log("finalArray", finalArray);

    await dispatch(saveExtractedData(finalArray));
    const generatedResult = generateExcel(finalArray, groupSmokeTestDomain.length);
    const result = (generatedResult === "Success") ? "Success" : "Failure";
    return result;
  };

   /** make the excel formatted data from json response */
   const exportToExcelForRegressionPack = async (exportData) => {
    if (exportData.length === 0) {
        return "Failure";
    }

    const flattenedData = exportData.flatMap(items => items.map(item => item.$));
    const testCaseData = exportData.flatMap(items => items.map(data => data.testcase));
    
    const transformItem = (item, data) => {
      const nameParts = item.name.split('-');
      console.log('data', item);
      // console.log('nameParts[1].trim()', nameParts[1].trim());
      // Handle Smoke Test Case Separately
      if (nameParts[1] === "Smoke Test" || nameParts[1] === "Regression"|| nameParts[1] === " Full Regression ") {
        return {
          Domain: nameParts[0],
          Type: nameParts[1],
          Application: nameParts[2],
          NoOfCase: (nameParts[2] === "Cap & Floor" || nameParts[2] === "Desk Adjuster") ? 1 : parseInt(item.tests),
          NoOfPass: (nameParts[2] === "Cap & Floor" || nameParts[2] === "Desk Adjuster") ? (parseInt(item.tests - item.failures) !== 0 ? 1 : 0) : item.tests - item.failures,
          NoOfFail: (nameParts[2] === "Cap & Floor" || nameParts[2] === "Desk Adjuster") ? (parseInt(item.tests - item.failures) !== 0 ? 0 : 1) : parseInt(item.failures),
          Environment: "ACCP01",
        };
      }
      else if (nameParts[2] === "VDFIN") {
        return {
          Domain: nameParts[0],
          Type: nameParts[1],
          Application: nameParts[2],
          NoOfCase: 102,
          NoOfPass: (parseInt(item.failures) == 84) ? 0 : 102,
          NoOfFail: (parseInt(item.failures) == 84) ? 102 : 0,
          Environment: "PROD",
        };
      }
    
      // Handle Other Cases
      const environmentData = {};
      //console.log(data);
      data?.forEach((testcase) => {
        const environment = testcase.$.name.split("|")[2].split("-")[0].trim();
        environmentData[environment] = environmentData[environment] || {
          count: 0,
          pass: 0,
          fail: 0,
        };
        environmentData[environment].count++;
        environmentData[environment].fail += testcase.failure ? 1 : 0;
        environmentData[environment].pass = environmentData[environment].count - environmentData[environment].fail;
      });
      //console.log("environmentData", environmentData);
         
      // Check for Matching Environment
      const environments = ["ACCP", "ACCP01", "ACCP02", "FORM", "PPRO", "TEST01", "PROD"];
      const results = [];
      for (const environment of environments) {
        if (environmentData[environment]) {
          results.push({
            Domain: nameParts[0],
            Type: nameParts[1],
            Application: nameParts[2],
            NoOfCase: environmentData[environment].count,
            NoOfPass: environmentData[environment].pass,
            NoOfFail: environmentData[environment].fail,
            Environment: (environment == "ACCP") ? "ACCP01" : environment,
          });
        }
      }
      return results;
    };
    
    const splittedItems = flattenedData.map((flattenedItem, index) => {
      const testCaseItem = testCaseData[index];
      return transformItem(flattenedItem, testCaseItem);
    });
  
    const finalArray = splittedItems.flat();
    console.log("finalArray", finalArray);
    
    //await dispatch(saveExtractedData(finalArray));
    const generatedResult = generateExcelForTestCases(finalArray);
    const result = (generatedResult === "Success") ? "Success" : "Failure";
    return result;
  };

  const handleExport = async(data) => {
    if(data === "rrt") {
    const jsonResponse = await dispatch(getXmltoJsonResponse());
    const convertedData = await exportToExcel(jsonResponse);
    console.log("convertedData", convertedData);
    } else {
      const jsonResponse = await dispatch(getRegressionResponse());
      const convertedData = await exportToExcelForRegressionPack(jsonResponse);
      console.log("jsonResponse", jsonResponse);
    }
  };
    
  return <>
  <button className="btn btn-success m-3" onClick={() => handleExport("rrt")}>Export to Excel</button>
  <button className="btn btn-success m-3" onClick={() => handleExport("regression")}>Export Testcase</button>
  <TestcaseExtract />
  </>
};

export default XMLDisplay;