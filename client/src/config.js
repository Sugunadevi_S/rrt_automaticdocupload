module.exports = {
    serviceUrl: "http://localhost:8000/",
    getAllFolders: "getAllFolders",
    getJsonResponse: "getJsonResponse",
    getRegressionResponse: "getRegressionResponse",
    insertData: "insertData",
    mailData: "send/mail",
    getFileResponse: "getFileResponse",
    insertTestCase: "insertTestCase"
}