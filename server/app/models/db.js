const mssql = require("mssql");
const dbConfig = require("../config/db.config.js");

const pool = new mssql.ConnectionPool({
  server: dbConfig.HOST,
  port: dbConfig.PORT,
  user: dbConfig.USER,
  password: dbConfig.PASSWORD,
  database: dbConfig.DB,
  options: {
    encrypt: true,
    trustServerCertificate: true, // Use this if you're connecting to Azure
  },
  pool: {
    max: 10,
    min: 0,
    idleTimeoutMillis: 30000,
  },  // Use this option cautiously
});

module.exports = pool;