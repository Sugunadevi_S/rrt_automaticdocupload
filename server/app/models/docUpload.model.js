const fs = require('fs');
var spauth = require('node-sp-auth');
//var spsave = require('spsave');
const request = require('request-promise');
const config = require('../../config');
const path = require("path");
var gulp = require("gulp");
var spsave = require("gulp-spsave");
// Add this line early in your code
//require('events').EventEmitter.defaultMaxListeners = 20; // or a higher number as needed
//myEmitter.setMaxListeners(20);

exports.GetReleaseFolders = async () => {
    try {
        const headeroptions = await spauth.getAuth(config.authURL, {
            clientId: config.clientId,
            clientSecret: config.clientSecret
        });
        
        const headers = {
            'content-type': 'application/json;odata=verbose',
            'Accept': 'application/json;odata=verbose',
            ...headeroptions.headers
        };
        
        const options = {
            method: 'GET',
            uri: config.getFoldersURL,
            headers,
        };
        const body = await request(options);
        const data = JSON.parse(body);
        const folders = data.d.Folders.results;
        return folders;
    } catch (error) {
        console.error("Error fetching folders:", error);
        throw error; // Re-throw the error to be handled in the route
    }
};

exports.UploadDocuments = async ( releaseName) => {
    sourcePath = [
        'G:/PRJ/SQS/Tosca Automation/Screenshots/Automatic_Update/HealthCheck',
        'G:/PRJ/SQS/Tosca Automation/Screenshots/Automatic_Update/Channels/Tango',
        'G:/PRJ/SQS/Tosca Automation/Screenshots/Automatic_Update/Channels/Vconnect',
        'G:/PRJ/SQS/Tosca Automation/Screenshots/Automatic_Update/Claims/ClaimCenter',
        'G:/PRJ/SQS/Tosca Automation/Screenshots/Automatic_Update/Claims/Desk Adjuster',
        'G:/PRJ/SQS/Tosca Automation/Screenshots/Automatic_Update/NLP'
    ]
    await sourcePath.map(async(filePath) => {
        console.log("filePath", filePath)
        const fileList = fs.readdirSync(filePath);
        console.log("fileList", fileList);
        let type = "",  domain = "";
        if (filePath.includes("HealthCheck"))
            type = "Healthcheck";
        else if (filePath.includes("Tango") || filePath.includes("Vconnect"))
            type = filePath.includes("Tango") ? "Tango" : "VConnect";
        else if (filePath.includes("ClaimCenter") || filePath.includes("Desk Adjuster"))
            type = filePath.includes("ClaimCenter") ? "Claim Center" : "Desk Adjuster";
        else if (filePath.includes("NLP"))
            type = "NLP";

        if (type === "Tango" || type === "VConnect")
            domain = "Channels";
        else if (type === "Claim Center" || type === "Desk Adjuster")
            domain = "Claims";

        // Create an array of promises for each file upload
         const uploadedFiles = await Promise.all(fileList.map(async (filename) => {
            const headeroptions = await spauth.getAuth(config.authURL, {
                clientId: config.clientId,
                clientSecret: config.clientSecret
            });

            const headers = {
                'Content-Type': 'application/json;odata=verbose',
                'Accept': 'application/json;odata=verbose',
                ...headeroptions.headers
              };

            const commonUrlPrefix = `GetFolderByServerRelativeUrl('CCB Deliverables/CCB Deliverables for Review/Test Deliverables/Logfiles testautomation/${releaseName}`;
            const commonUrlSuffix =  `/R4ACC')/Files/add(url='${filename}',overwrite=true)`;
            const typeSpecificUrlPart = (type === "Tango" || type === "VConnect" || type === "Desk Adjuster" || type === "Claim Center")
                ? `/${domain}/${type}`
                : `/${type}`; 
            
            const options = {
                method: 'POST',
                uri: config.targetPath + commonUrlPrefix + typeSpecificUrlPart + commonUrlSuffix,
                __metadata: { "type": "SP.User" },
                formData: {
                    name: 'Test',
                    file: {
                        value: fs.createReadStream(`${filePath}/${filename}`),
                        options: {
                            filename: filename,
                            contentType: 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'
                          }
                    }
                },
                headers,
            };

            const body = await request(options);
            return body;
           
         }));
    });

        //const successCount = uploadedFiles.filter((result) => result !== 'null' && result !== 'undefined').length;
        // const result = successCount === fileList.length
        //     ? "All documents are uploaded successfully"
        //     : `Some documents failed to upload (${successCount} out of ${fileList.length})`;
        const result = "All documents are uploaded successfully";

        return result;
};


const getSourcePaths = () => [
    'G:/PRJ/SQS/Tosca Automation/Screenshots/Automatic_Update/HealthCheck',
    'G:/PRJ/SQS/Tosca Automation/Screenshots/Automatic_Update/Channels/Tango',
    'G:/PRJ/SQS/Tosca Automation/Screenshots/Automatic_Update/Channels/Vconnect',
    'G:/PRJ/SQS/Tosca Automation/Screenshots/Automatic_Update/Claims/ClaimCenter',
    'G:/PRJ/SQS/Tosca Automation/Screenshots/Automatic_Update/Claims/Desk Adjuster',
    'G:/PRJ/SQS/Tosca Automation/Screenshots/Automatic_Update/NLP'
];

const getTypeAndDomain = (filePath) => {
    let type = '', domain = '';

    if (filePath.includes('HealthCheck')) type = 'Healthcheck';
    else if (filePath.includes('Tango') || filePath.includes('Vconnect')) type = filePath.includes('Tango') ? 'Tango' : 'VConnect';
    else if (filePath.includes('ClaimCenter') || filePath.includes('Desk Adjuster')) type = filePath.includes('ClaimCenter') ? 'Claim Center' : 'Desk Adjuster';
    else if (filePath.includes('NLP')) type = 'NLP';

    if (type === 'Tango' || type === 'VConnect') domain = 'Channels';
    else if (type === 'Claim Center' || type === 'Desk Adjuster') domain = 'Claims';

    return { type, domain };
};

const getTypeSpecificUrlPart = (type, domain) => {
    return (type === 'Tango' || type === 'VConnect' || type === 'Desk Adjuster' || type === 'Claim Center')
        ? `/${domain}/${type}`
        : `/${type}`;
};

const getDestinationFolderPath = (releaseName, typeSpecificUrlPart) => {
    console.log("typeSpecificUrlPart", typeSpecificUrlPart);
    return `CCB Deliverables/CCB Deliverables for Review/Test Deliverables/Logfiles testautomation/${releaseName}${typeSpecificUrlPart}/R4ACC`;
};

const getAuthOptions = () => {
    return {
        clientId: config.clientId,
        clientSecret: config.clientSecret,
    };
};

const uploadFile = async (siteUrl, destinationFolderPath, filename, filepath) => {
    const auth = await spauth.getAuth(siteUrl, getAuthOptions());

    const spsaveOptions = {
        siteUrl,
        folder: destinationFolderPath,
        fileName: filename,
        fileContent: await fs.readFile(filepath),
        auth,
        checkin: true,
        checkinType: 1,
        flatten: false,
        notification: true
    };
    console.log("filepath", filepath);
    return new Promise((resolve, reject) => {
        gulp.src(filepath)
            .pipe(spsave(spsaveOptions))
            .on('end', () => {
                console.log(`File "${filename}" uploaded successfully.`);
                resolve();
            })
            .on('error', (err) => {
                console.error(`Error uploading file "${filename}":`, err);
                reject(err);
            });
    });
};

// exports.UploadDocuments = async (releaseName) => {
//     const sourcePaths = getSourcePaths();

//     await sourcePaths.map(async (filePath) => {
//         console.log('Processing files from:', filePath);

//         const fileList = await fs.readdir(filePath);
//         console.log("fileList", fileList);

//         const { type, domain } = getTypeAndDomain(filePath);
//         const typeSpecificUrlPart = getTypeSpecificUrlPart(type, domain);

//         const siteUrl = config.authURL;
//         const destinationFolderPath = getDestinationFolderPath(releaseName, typeSpecificUrlPart);

//         await Promise.all(fileList.map(async (filename) => {
//             const filepath = path.resolve(filePath, filename);
//             await uploadFile(siteUrl, destinationFolderPath, filename, filepath);
//         }));
        
//     });
//     result = `Finished processing files`;
//     return result;
// };
