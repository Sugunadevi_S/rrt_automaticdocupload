const pool = require("./db");
const mssql = require('mssql');
const util = require("util");
const xml2js = require('xml2js');
const fsPromises = require('fs').promises;
const path = require('path');

exports.InsertReportData = (insertValues) =>{
    pool.connect()
        .then(() => {
            console.log('Connected to MSSQL database');
            insertValues.map((data) => {
                const req = new mssql.Request(pool);
                let sql = "INSERT INTO automaticReport (Domain, Type, Application, NoOfCases, NoOfPass, NoOfFail, Environment) VALUES ";
                sql += util.format("('%s', '%s', '%s', %d, %d, %d, '%s')", data.Domain, data.Type, data.Application, data.NoOfCase, data.NoOfPass, data.NoOfFail, data.Environment);
                req.query(sql)
                    .then(result => {
                        return result;
                    })
                    .catch(err => {
                        console.error('Error inserting record:', err);
                    })
            })
        })
        .catch((err) => {
            console.error('Error connecting to MSSQL database:', err);
        });
}


function getLatestFile(files) {
    let latestDate = null;
    let latestNumber = null;
    let latestFile = null;

    files.forEach((file) => {
        const match = file.match(/(\d{2})(\d{2})(\d{4})_(\d+)\.xml/); // Assuming the format is DDMMYYYY_number.xml
        if (match) {
            const day = parseInt(match[1], 10);
            const month = parseInt(match[2], 10) - 1; // Subtract 1 from the month as it's zero-based
            const year = parseInt(match[3], 10);
            const number = parseInt(match[4], 10);

            const fileDate = new Date(year, month, day);

            if (!latestDate || fileDate > latestDate || (fileDate.getTime() === latestDate.getTime() && number > latestNumber)) {
                latestDate = fileDate;
                latestNumber = number;
                latestFile = file;
            }
        }
    });
    console.log("latestFile", latestFile);
    return latestFile;
}


exports.getData = async () => {
    try {
        const folderPath = [
        '//pvspta01/ToscaCI/reports/Regression smoke packs/DCC',
        '//pvspta01/ToscaCI/reports/Regression smoke packs/Channels/Tango',
        '//pvspta01/ToscaCI/reports/Regression smoke packs/Channels/VConnect',
        '//pvspta01/ToscaCI/reports/Regression smoke packs/Claims/ClaimCenter',
        '//pvspta01/ToscaCI/reports/Regression smoke packs/Claims/DeskAdjuster',
        '//pvspta01/ToscaCI/reports/Regression smoke packs/NonLife/NLP',
        '//pvspta01/ToscaCI/reports/Regression smoke packs/NonLife/Cap',
        '//pvspta01/ToscaCI/reports/Health Checks/Health Checks',
        '//pvspta01/ToscaCI/reports/Health Checks/SingleIDP'
        ];

    
    const filteredFiles = [];
    await Promise.all(folderPath.map(async (folder) => {
        try {
            const extractFile = await fsPromises.readdir(folder);
            const latestFile = await getLatestFile(extractFile);
            if (latestFile) {
            const filePath = path.join(folder, latestFile);
            filteredFiles.push(filePath);
            } else {
            console.log("No valid files found in the directory.");
            }
        } catch (error) {
            console.error(`Error reading files from folder ${folder}:`, error);
        }
        }));
    if (filteredFiles.length === 0) {
            return "No files found for the given date.";
        }

        const fileDataPromises = filteredFiles.map(async (dataFile) => {
        const xmlData = await fsPromises.readFile(dataFile, 'utf8');
        return xml2js.parseStringPromise(xmlData);
    });
    
    const jsonResults = await Promise.all(fileDataPromises);
    
    const result = jsonResults.map((result) => result.testsuites.testsuite);
    return result;
    } catch (error) {
        return error;
        
    }
};

exports.getRegressionData = async () => {
    try {
        const folderPath = [
        '//pvspta01/ToscaCI/reports/Regression smoke packs/Inflow',
        '//pvspta01/ToscaCI/reports/Regression smoke packs/QuickWins',
        '//pvspta01/ToscaCI/reports/Regression smoke packs/LifeConnect Smoke',
        '//pvspta01/ToscaCI/reports/Regression smoke packs/Channels/Tango',
        '//pvspta01/ToscaCI/reports/Regression smoke packs/Channels/VConnect',
        //'//pvspta01/ToscaCI/reports/Regression smoke packs/Claims/ClaimCenter',
        '//pvspta01/ToscaCI/reports/Regression smoke packs/Claims/DeskAdjuster',
        '//pvspta01/ToscaCI/reports/Regression smoke packs/NonLife/NLP',
        '//pvspta01/ToscaCI/reports/Regression smoke packs/NonLife/Cap',
        '//pvspta01/ToscaCI/reports/Health Checks/SingleIDP',
        '//pvspta01/ToscaCI/reports/Health Checks/Health Checks'

    ];

    
    const filteredFiles = [];
    await Promise.all(folderPath.map(async (folder) => {
        try {
            const extractFile = await fsPromises.readdir(folder);
            const latestFile = await getLatestFile(extractFile);
            if (latestFile) {
            const filePath = path.join(folder, latestFile);
            filteredFiles.push(filePath);
            } else {
            console.log("No valid files found in the directory.");
            }
        } catch (error) {
            console.error(`Error reading files from folder ${folder}:`, error);
        }
        }));
    if (filteredFiles.length === 0) {
            return "No files found for the given date.";
        }

        const fileDataPromises = filteredFiles.map(async (dataFile) => {
        const xmlData = await fsPromises.readFile(dataFile, 'utf8');
        return xml2js.parseStringPromise(xmlData);
    });
    
    const jsonResults = await Promise.all(fileDataPromises);

    const result = jsonResults.map((result) => result.testsuites.testsuite);
    return result;
    } catch (error) {
        return error;
        
    }
};

exports.InsertTestCaseData = (insertValues) =>{
    pool.connect()
        .then(() => {
            console.log('Connected to MSSQL database');
            insertValues.map((data) => {
                const req = new mssql.Request(pool);
                let sql = "INSERT INTO rrtResult (Domain, Type, Application, TestCase, Status, Environment) VALUES ";
                sql += util.format("('%s', '%s', '%s', '%s', '%s', '%s')", data.Domain, data.Type, data.Application, data.TestCase, data.Status, data.Environment);
                req.query(sql)
                    .then(result => {
                        return result;
                    })
                    .catch(err => {
                        console.error('Error inserting record:', err);
                    })
            })
        })
        .catch((err) => {
            console.error('Error connecting to MSSQL database:', err);
        });
}