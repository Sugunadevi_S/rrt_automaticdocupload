const docUpload = require("../models/docUpload.model");

exports.GetAllFolders = async (req, res) => {
    try {
        const folders = await docUpload.GetReleaseFolders();
        res.send({
            status: "SUCCESS",
            messageCode: "MSG200",
            message: "User DB Connection Updated Successfully",
            userMessage: "User DB Connection Updated Successfully !!!.",
            folders
          });
    } catch (error) {
        res.status(500).send({
            status: "Failure",
            messageCode: "MSG500",
            message: "Some error occurred while Updating records",
            userMessage: "Connection error : User DB Connection status Updating Failed!!!",
        });
    }
};

exports.uploadDocument = async (req, res) => {
    try {
        const result = await docUpload.UploadDocuments(req.params.releaseName);
        res.send({
            status: "SUCCESS",
            messageCode: "MSG200",
            message: "User DB Connection Updated Successfully",
            userMessage: "User DB Connection Updated Successfully !!!.",
            result
          });
    } catch (error) {
        res.status(500).send({
            status: "Failure",
            messageCode: "MSG500",
            message: "Some error occurred while Updating records",
            userMessage: "Connection error : User DB Connection status Updating Failed!!!",
          });
    }
};