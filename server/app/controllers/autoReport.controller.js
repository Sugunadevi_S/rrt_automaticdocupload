const autoReport = require("../models/autoReport.model");

exports.insertData =  async (req, res) => {
  try {
    const result = await autoReport.InsertReportData(req.body);
    res.send({
      status: "SUCCESS",
      messageCode: "MSG200",
      message: "User DB Connection Updated Successfully",
      userMessage: "User DB Connection Updated Successfully !!!.",
      result
    });
  } catch (e) {
    res.status(500).send({
      status: "Failure",
      messageCode: "MSG500",
      message: "Some error occurred while Updating records",
      userMessage:
        "Connection error : User DB Connection status Updating Failed!!!",
    });
  }
};

exports.getJsonData =  async (req, res) => {
  try {
    const result = await autoReport.getData();
    res.send({
      status: "SUCCESS",
      messageCode: "MSG200",
      message: "User DB Connection Updated Successfully",
      userMessage: "User DB Connection Updated Successfully !!!.",
      result
    });
  } catch (e) {
    res.status(500).send({
      status: "Failure",
      messageCode: "MSG500",
      message: "Some error occurred while Updating records",
      userMessage:"Connection error : User DB Connection status Updating Failed!!!",
    });
  }
};

exports.getRegressionData =  async (req, res) => {
  try {
    const result = await autoReport.getRegressionData();
    res.send({
      status: "SUCCESS",
      messageCode: "MSG200",
      message: "User DB Connection Updated Successfully",
      userMessage: "User DB Connection Updated Successfully !!!.",
      result
    });
  } catch (e) {
    res.status(500).send({
      status: "Failure",
      messageCode: "MSG500",
      message: "Some error occurred while Updating records",
      userMessage:"Connection error : User DB Connection status Updating Failed!!!",
    });
  }
};

exports.getJsonFileData =  async (req, res) => {
  try {
    const result = await autoReport.getJsonData();
    res.send({
      status: "SUCCESS",
      messageCode: "MSG200",
      message: "User DB Connection Updated Successfully",
      userMessage: "User DB Connection Updated Successfully !!!.",
      result
    });
  } catch (e) {
    res.status(500).send({
      status: "Failure",
      messageCode: "MSG500",
      message: "Some error occurred while Updating records",
      userMessage:"Connection error : User DB Connection status Updating Failed!!!",
    });
  }
};

exports.insertTestCase =  async (req, res) => {
  try {
    const result = await autoReport.InsertTestCaseData(req.body);
    res.send({
      status: "SUCCESS",
      messageCode: "MSG200",
      message: "User DB Connection Updated Successfully",
      userMessage: "User DB Connection Updated Successfully !!!.",
      result
    });
  } catch (e) {
    res.status(500).send({
      status: "Failure",
      messageCode: "MSG500",
      message: "Some error occurred while Updating records",
      userMessage:
        "Connection error : User DB Connection status Updating Failed!!!",
    });
  }
};