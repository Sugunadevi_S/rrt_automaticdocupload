const docUpload = require("../controllers/docUpload.controller");

module.exports = (app) => {
    // Get db connection for all users
    app.get(
        "/getAllFolders", docUpload.GetAllFolders
    );
  
    // Create user db connection from add db connection popup
    app.post(
      "/documentUpload/:releaseName", docUpload.uploadDocument
    );
};