const autoReport = require("../controllers/autoReport.controller");

module.exports = (app) => {
  app.get(
    "/getJsonResponse",
    autoReport.getJsonData
  );
  app.post(
    "/insertData",
    autoReport.insertData
  );
  app.get(
    "/getRegressionResponse",
    autoReport.getRegressionData
  );
  app.post(
    "/insertTestCase",
    autoReport.insertTestCase
  );
  // app.get(
  //   "/getFileResponse",
  //   autoReport.getJsonFileData
  // );
}